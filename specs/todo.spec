# Todo app Acceptance
* Open "http://34.118.32.103"

## Scenario When I write buy some milk I should see the buy some milk in my list
* Given empty todo list
* When I write "buy some milk" to "Enter Todo" textbox and click "Add" button
* Then I should see "buy some milk" item in the todo list
