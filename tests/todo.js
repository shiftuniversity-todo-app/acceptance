const assert = require('assert');
const { write, into, text, $, waitFor, click } = require('taiko');

step('Given empty todo list', async () => {
	assert.ok(await $('#todo-list').exists());
});

step(
	'When I write <text> to <input> textbox and click <button> button',
	async (text, input, button) => {
		await write(text, into($('#todo-textbox')));
		await waitFor(3000);
		await click(button);
	},
);

step('Then I should see <write> item in the todo list', async (write) => {
	assert.ok(await text(write).exists());
});
